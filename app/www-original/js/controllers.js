'use strict';
// jshint camelcase:false

var API_BASE = 'https://limitless-headland-6808.herokuapp.com/';
// var API_BASE = 'http://localhost:3000/';
var _ = window._;

function numberOfDays(year, month) {
  var d = new Date(year, month, 0);
  return d.getDate();
}

angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $q, $http, $ionicModal, $localstorage, $state, $ionicLoading, UserService, FACEBOOK_APP_ID) {

  var fbLogged = $q.defer();

  // This is the success callback from the login method
  var fbLoginSuccess = function(response) {
    if (!response.authResponse){
      fbLoginError("Cannot find the authResponse");
      return;
    }
    var expDate = new Date(
      new Date().getTime() + response.authResponse.expiresIn * 1000
    ).toISOString();

    var authData = {
      id: String(response.authResponse.userID),
      access_token: response.authResponse.accessToken,
      expiration_date: expDate
    }
    fbLogged.resolve(authData);
  };

  //This is the fail callback from the login method
  var fbLoginError = function(error){
    fbLogged.reject(error);
    alert(error);
    $ionicLoading.hide();
  };

  // This method is to get the user profile info from the facebook api
  var getFacebookProfileInfo = function () {
    var info = $q.defer();
    facebookConnectPlugin.api('/me', "",
      function (response) {
        info.resolve(response);
      },
      function (response) {
        info.reject(response);
      }
    );
    return info.promise;
  };

  // This method is executed when the user press the "Login with facebook" button
  $scope.login = function() {

    if (!window.cordova) {
      //this is for browser only
      facebookConnectPlugin.browserInit(FACEBOOK_APP_ID);
    }

    //check if we have user's data stored
    var user = UserService.getUser();

    facebookConnectPlugin.getLoginStatus(function(success){
      // alert(success.status);
     if(success.status === 'connected'){
        // the user is logged in and has authenticated your app, and response.authResponse supplies
        // the user's ID, a valid access token, a signed request, and the time the access token
        // and signed request each expire
        // $state.go('app.feed');
        console.log('logged in');

     } else {
        console.log('not logged in');
        //if (success.status === 'not_authorized') the user is logged in to Facebook, but has not authenticated your app
        //else The person is not logged into Facebook, so we're not sure if they are logged into this app or not.

        //this is a loader
        $ionicLoading.show({
          template: 'Logging in...'
        });

        //ask the permissions you need
        //you can learn more about FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.2
        facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);

        fbLogged.promise.then(function(authData) {

          var fb_uid = authData.id,
              fb_access_token = authData.access_token;

            //get user info from FB
            getFacebookProfileInfo().then(function(data) {

              var user = data;
              user.picture = "http://graph.facebook.com/"+fb_uid+"/picture?type=large";
              user.access_token = fb_access_token;

              //save the user data
              //for the purpose of this example I will store it on ionic local storage but you should save it on a database
              UserService.setUser(user);

              $ionicLoading.hide();
              // $state.go('app.feed');
              console.log('got data from fb');
            });
        });
      }
    });
  };

  $scope.isLoggedIn = function () {
    if($localstorage.getObject('user') && $localstorage.getObject('user').access_token) {
      return true;
    } else {
      return false;
    }
  };

  if($scope.isLoggedIn()) {
    $http.defaults.headers.common.Authorization = 'Bearer ' + $localstorage.getObject('user').access_token;
  }

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.hideLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.showLogin = function() {
    if(!$scope.isLoggedIn()) {
      $scope.modal.show();
    }
  };

  // // Perform the login action when the user submits the login form
  // $scope.login = function() {
  //   // $scope.loginData.password = $.sha256($scope.loginData.password);

  //   // $http.get(API_BASE + 'authorize', {
  //   //   headers: { 'Content-Type': 'application/json' },
  //   //   params: $scope.loginData
  //   // })
  //   // .success(function(data, status, headers, config) {
  //   //   $scope.loggedIn(data);
  //   // })
  //   // .error(function(data, status, headers, config) {
  //   //   $scope.cannotLogin();
  //   // });
  // };

  $scope.cannotLogin = function() {
    $scope.hideLogin();
  };

  $scope.loggedIn = function(user) {
    $scope.hideLogin();
    $localstorage.setObject('user', user);
    $http.defaults.headers.common.Authorization = 'Bearer ' + user.access_token;
  };

  $scope.logout = function() {
    $localstorage.setObject('user', null);
  };

})

.controller('StoriesCtrl', function($scope, $http, $ionicSlideBoxDelegate) {
  $ionicSlideBoxDelegate.update();
  $http.get(API_BASE + 'stories?status=active&count=5')
    .success(function(data) {
      $scope.stories = data.items;
      $ionicSlideBoxDelegate.update();
  });
})

.controller('SingleStoryCtrl', function($scope, $http, $stateParams) {
  $http.get(API_BASE + 'stories/' + $stateParams.id)
    .success(function(data) {
      $scope.story = data;
  });
})

.controller('ContactsCtrl', function($scope, $http) {
  $scope.search = function (e) {
    var filter;
    if(e) { filter = angular.element(e.currentTarget).val(); }

    var apiPath = API_BASE + 'contacts?';

    if(filter) {
      apiPath += 'full_name=' + filter;
    } else {
      apiPath += 'count=50';
    }

    $http.get(apiPath)
      .success(function(data) {
      $scope.contacts = data.items;
    });
  };

  $scope.search();
})

.controller('HallCtrl', function($scope, $http) {
  var d = new Date(),
      days = ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
      dayOfTheWeek = d.getDay(),
      today = d.getDate();

  $http.get(API_BASE + 'foodlists?status=active')
    .success(function(data) {
      $scope.days = data.items[0].days.slice(today - 1, numberOfDays(d.getYear(), d.getMonth() + 1));
      $scope.today = today;
      $scope.daysOfTheWeek = days;
      $scope.dayOfTheWeek = dayOfTheWeek;
      $scope.month = data.items[0].title;
    });
})

.controller('PostsCtrl', function($scope, $http, $stateParams) {
  var apiUrl = API_BASE + 'topics?count=50';

  if($stateParams.category) {
    apiUrl += '&category=' + $stateParams.category;
    $scope.category = $stateParams.category;
  }

  $http.get(apiUrl).success(function(data) {
    $scope.posts = data.items;
  });
})

.controller('NewPostCtrl', function($scope, $http, $state, $stateParams, $localstorage) {
  $scope.me = $localstorage.getObject('user');
  $scope.post = {};

  $scope.setCategory = function (category) {
    if(category == $scope.post.category) {
      $scope.post.category = null;
      $('.category-selector').slideDown(150);
    } else {
      $scope.post.category = category;
      $('.category-selector[data-category!="' + category + '"]').slideUp(150);
    }
  };

  $scope.add = function () {
    if($scope.isLoggedIn()) {
      if($scope.post.category && $scope.post.title && $scope.post.details) {
        $http.post(API_BASE + 'topics/', $scope.post)
          .success(function(data) {
            $state.go('app.post', { id : data.index });
        });
      }
    } else {
      $scope.showLogin();
    }
  };

  if($stateParams.category) {
    $scope.setCategory($stateParams.category);
  }
})

.controller('CalendarCtrl', function($scope, $http, $stateParams) {
  // Get current date
  $scope.d = new Date();

  $scope.init = function () {
    var mon = 'Pzt';
    var tue = 'Sal';
    var wed = 'Çrş';
    var thur = 'Prş';
    var fri = 'Cum';
    var sat = 'Cts';
    var sund = 'Paz';

    // Get current month and set as '.current-month' in title
    $scope.month = $scope.d.getMonth() + 1;

    $scope.setMonth($scope.month, mon, tue, wed, thur, fri, sat, sund);

    $('.btn-next').on('click', function () {
      if ($scope.month > 11) {
        $scope.month = 0;
      }
      $scope.setMonth(parseInt($scope.month) + 1, mon, tue, wed, thur, fri, sat, sund);
    });

    $('.btn-prev').on('click', function () {
      if ($scope.month < 2) {
        $scope.month = 13;
      }
      $scope.setMonth(parseInt($scope.month) - 1, mon, tue, wed, thur, fri, sat, sund);
    });

    // Add class '.active' on calendar date
    $('tbody td').on('click', function() {
      if ($(this).hasClass('event')) {
        $('tbody.event-calendar td').removeClass('active');
        $(this).addClass('active');
      } else {
        $('tbody.event-calendar td').removeClass('active');
      }
    });
  };

  // Get current day and set as '.current-day'
  $scope.setCurrentDay = function(month) {
    var viewMonth = $scope.month;
    if (parseInt(month) === parseInt(viewMonth)) {
      $('tbody.event-calendar td[date-day="' + $scope.d.getDate() + '"]').addClass('current-day');
    }
  };

  // Get all dates for current month
  $scope.printDateNumber = function(monthNumber, mon, tue, wed, thur, fri, sat, sund) {
    $($('tbody.event-calendar tr')).each(function() {
      $(this).empty();
    });

    $($('thead.event-days tr')).each(function() {
      $(this).empty();
    });

    function getDaysInMonth(month, year) {
      // Since no month has fewer than 28 days
      var date = new Date(year, month, 1);
      var days = [];
      while (date.getMonth() === month) {
        days.push(new Date(date));
        date.setDate(date.getDate() + 1);
      }
      return days;
    }

    var yearNumber = (new Date()).getFullYear();
    var i = 0;

    setDaysInOrder(mon, tue, wed, thur, fri, sat, sund);

    function setDaysInOrder(mon, tue, wed, thur, fri, sat, sund) {
      var monthDay = getDaysInMonth(monthNumber - 1, yearNumber)[0].toString().substring(0, 3);
      if (monthDay === 'Mon') {
        $('thead.event-days tr').append('<td>' + mon + '</td><td>' + tue + '</td><td>' + wed + '</td><td>' + thur + '</td><td>' + fri + '</td><td>' + sat + '</td><td>' + sund + '</td>');
      } else if (monthDay === 'Tue') {
        $('thead.event-days tr').append('<td>' + tue + '</td><td>' + wed + '</td><td>' + thur + '</td><td>' + fri + '</td><td>' + sat + '</td><td>' + sund + '</td><td>' + mon + '</td>');
      } else if (monthDay === 'Wed') {
        $('thead.event-days tr').append('<td>' + wed + '</td><td>' + thur + '</td><td>' + fri + '</td><td>' + sat + '</td><td>' + sund + '</td><td>' + mon + '</td><td>' + tue + '</td>');
      } else if (monthDay === 'Thu') {
        $('thead.event-days tr').append('<td>' + thur + '</td><td>' + fri + '</td><td>' + sat + '</td><td>' + sund + '</td><td>' + mon + '</td><td>' + tue + '</td><td>' + wed + '</td>');
      } else if (monthDay === 'Fri') {
        $('thead.event-days tr').append('<td>' + fri + '</td><td>' + sat + '</td><td>' + sund + '</td><td>' + mon + '</td><td>' + tue + '</td><td>' + wed + '</td><td>' + thur + '</td>');
      } else if (monthDay === 'Sat') {
        $('thead.event-days tr').append('<td>' + sat + '</td><td>' + sund + '</td><td>' + mon + '</td><td>' + tue + '</td><td>' + wed + '</td><td>' + thur + '</td><td>' + fri + '</td>');
      } else if (monthDay === 'Sun') {
        $('thead.event-days tr').append('<td>' + sund + '</td><td>' + mon + '</td><td>' + tue + '</td><td>' + wed + '</td><td>' + thur + '</td><td>' + fri + '</td><td>' + sat + '</td>');
      }
    }

    $(getDaysInMonth(monthNumber - 1, yearNumber)).each(function(index) {
      var index = index + 1;
      if (index < 8) {
        $('tbody.event-calendar tr.1').append('<td date-month="' + monthNumber + '" date-day="' + index + '">' + index + '</td>');
      } else if (index < 15) {
        $('tbody.event-calendar tr.2').append('<td date-month="' + monthNumber + '" date-day="' + index + '">' + index + '</td>');
      } else if (index < 22) {
        $('tbody.event-calendar tr.3').append('<td date-month="' + monthNumber + '" date-day="' + index + '">' + index + '</td>');
      } else if (index < 29) {
        $('tbody.event-calendar tr.4').append('<td date-month="' + monthNumber + '" date-day="' + index + '">' + index + '</td>');
      } else if (index < 32) {
        $('tbody.event-calendar tr.5').append('<td date-month="' + monthNumber + '" date-day="' + index + '">' + index + '</td>');
      }
      i++;
    });
    var date = new Date();
    var month = date.getMonth() + 1;
    $scope.setCurrentDay(month);
    $scope.displayEvent();
  };

  $scope.getMonthName = function(monthNumber) {
    var months = ['OCAK', 'ŞUBAT', 'MART', 'NİSAN', 'MAYIS', 'HAZİRAN', 'TEMMUZ', 'AĞUSTOS', 'EYLÜL', 'EKİM', 'KASIM', 'ARALIK'];
    return months[monthNumber - 1];
  };

  $scope.setMonth = function(monthNumber, mon, tue, wed, thur, fri, sat, sund) {
    $('.month').text($scope.getMonthName(monthNumber));
    $scope.month = monthNumber;
    $scope.printDateNumber(monthNumber, mon, tue, wed, thur, fri, sat, sund);
    $scope.setEventsForMonth();
  };

  // Get current day on click in calendar and find day-event to display
  $scope.displayEvent = function() {
    $('tbody.event-calendar td').on('click', function() {
      $('.day-event').slideUp('fast');
      var monthEvent = $(this).attr('date-month');
      var dayEvent = $(this).text();
      $('.day-event[date-month="' + monthEvent + '"][date-day="' + dayEvent + '"]').slideDown('fast');
    });
  };

  // Add '.event' class to all days that has an event
  $scope.setEvent = function () {
    $('.day-event').each(function() {
      var eventMonth = $(this).attr('date-month');
      var eventDay = $(this).attr('date-day');
      $('tbody.event-calendar tr td[date-month="' + eventMonth + '"][date-day="' + eventDay + '"]').addClass('event');
    });
  };

  $scope.events = {};
  $scope.category = 'other';

  $scope.setCategory = function ($event) {
    $('tbody.event-calendar tr td').removeClass('event');

    $($event.currentTarget)
      .addClass('active')
      .siblings()
        .removeClass('active');
    $scope.category = $($event.currentTarget).data('category');
    $scope.setEventsForMonth();
  };

  $scope.setEventsForMonth = function () {
    var y = $scope.d.getFullYear();

    $http.get(API_BASE + 'events/' + y + '/' + $scope.month + '?category=' + $scope.category).success(function(data) {
      data.items = _.map(data.items, function (item) {
        item.d = new Date(item.start_time).getDate();
        item.m = new Date(item.start_time).getMonth() + 1;
        return item;
      });
      $scope.events = data.items;
      setTimeout(function () {
        $scope.setEvent();
      },0);
    });
  };

  $scope.init();
  $scope.setEventsForMonth();

})

.controller('SingleEventCtrl', function($scope, $http, $stateParams) {
  $http.get(API_BASE + 'events/' + $stateParams.id)
    .success(function(data) {
      if(data.category === 'student_club') {
        data.category_def = 'Kulüp Etkinliği';
      } else if(data.category === 'social') {
        data.category_def = 'Sosyal';
      } else if(data.category === 'academic') {
        data.category_def = 'Akademik';
      }
      $scope.event = data;
  });
})

.controller('SinglePostCtrl', function($scope, $http, $stateParams, $localstorage) {
  $scope.comment = {};

  $http.get(API_BASE + 'topics/' + $stateParams.id).success(function(data) {
    $scope.post = data;
  });

  $scope.like = function (dislike) {
    if($scope.isLoggedIn()) {
      var isDislike = false || dislike;
      $http.post(API_BASE + 'topics/' + $stateParams.id + '/like', { dislike: isDislike })
        .success(function(data) {
          $scope.post.liked_by.length = data[0];
          $scope.post.disliked_by.length = data[1];
      });
    } else {
      $scope.showLogin();
    }
  };

  $scope.addComment = function () {
    if($scope.isLoggedIn()) {
      if($scope.comment.details) {
        $http.post(API_BASE + 'topics/' + $stateParams.id + '/comments', $scope.comment)
          .success(function(data) {
            $scope.post.comments = data;
        });
      }
    } else {
      $scope.showLogin();
    }
  };


  $scope.likeComment = function (commentId, dislike) {
    if($scope.isLoggedIn()) {
      var isDislike = false || dislike;
      $http.post(API_BASE + 'topics/' + $stateParams.id + '/comments/'+ commentId +'/like', { dislike: isDislike })
        .success(function(data) {
          var comment = _.find($scope.post.comments, function (comment) {
            return comment._id.toString() === commentId.toString();
          });
          comment.liked_by.length = data[0];
          comment.disliked_by.length = data[1];
      });
    } else {
      $scope.showLogin();
    }
  };
});
