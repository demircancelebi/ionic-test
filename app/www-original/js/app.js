'use strict';

// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'ngCordova'])


.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }
  }
}])

.filter('moment', function() {
  moment.locale('tr-TR');
  return function(dateString) {
    return moment(dateString).fromNow();
  };
})

.filter('datetime', function() {
  moment.locale('tr-TR');
  return function(dateString) {
    return moment(dateString).calendar();
  };
})

.config(function($stateProvider, $stateParamsProvider, $urlRouterProvider, $cordovaFacebookProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.stories', {
    url: '/stories',
    views: {
      'menuContent': {
        templateUrl: 'templates/stories.html',
        controller: 'StoriesCtrl'
      }
    }
  })

  .state('app.single_story', {
    url: '/stories/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/single_story.html',
        controller: 'SingleStoryCtrl'
      }
    }
  })

  .state('app.calendar', {
    url: '/calendar',
    views: {
      'menuContent': {
        templateUrl: 'templates/calendar.html',
        controller: 'CalendarCtrl'
      }
    }
  })

  .state('app.posts', {
    url: '/posts?category',
    views: {
      'menuContent': {
        templateUrl: 'templates/posts.html',
        controller: 'PostsCtrl'
      }
    }
  })

  .state('app.new_post', {
    url: '/posts/new?category',
    views: {
      'menuContent': {
        templateUrl: 'templates/new_post.html',
        controller: 'NewPostCtrl'
      }
    }
  })

  .state('app.post', {
    url: '/posts/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/post.html',
        controller: 'SinglePostCtrl'
      }
    }
  })

  .state('app.categories', {
    url: '/categories',
    views: {
      'menuContent': {
        templateUrl: 'templates/posts_categories.html'
      }
    }
  })

  .state('app.about', {
    url: '/about',
    views: {
      'menuContent': {
        templateUrl: 'templates/about.html'
      }
    }
  })

  .state('app.cdetail', {
    url: '/events/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/single_event.html',
        controller: 'SingleEventCtrl'
      }
    }
  })

  .state('app.contacts', {
    url: '/contacts',
    views: {
      'menuContent': {
        templateUrl: 'templates/contacts.html',
        controller: 'ContactsCtrl'
      }
    }
  })

  .state('app.hall', {
    url: '/hall',
    views: {
      'menuContent': {
        templateUrl: 'templates/hall.html',
        controller: 'HallCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/stories');
});
